package practice.componentfilter.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import practice.componentfilter.component.Component;
import practice.componentfilter.component.PriceComponent;

class ComponentFilterServiceTest {
    private static final List<Component> COMPONENTS = List.of(
            new PriceComponent(10, "1"),
            new PriceComponent(20, "2"),
            new PriceComponent(30, "1")
    );
    private static final String CATEGORY_ID = "1";

    private final ComponentFilterService underTest = new ComponentFilterService();

    @Test
    void testCalculateComponentSumForCategoryIdShouldReturnZeroWhenCalledWithEmptyList() {

        int actual = underTest.calculateComponentSumForCategoryId(List.of(), CATEGORY_ID);

        assertEquals(0, actual);
    }

    @Test
    void testCalculateComponentSumForCategoryIdShouldReturnCategorySumWhenCalledPopulatedList() {

        int actual = underTest.calculateComponentSumForCategoryId(COMPONENTS, CATEGORY_ID);

        assertEquals(40, actual);
    }
}