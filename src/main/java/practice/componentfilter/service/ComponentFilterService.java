package practice.componentfilter.service;

import java.util.List;

import practice.componentfilter.component.Component;

public class ComponentFilterService {

    /**
     * Summarize the amounts for the given category id.
     * @param components component list
     * @param categoryId category id
     * @return sum of the amounts for the given category id
     */
    public Integer calculateComponentSumForCategoryId(List<Component> components, String categoryId) {
        return 0;
    }

}
