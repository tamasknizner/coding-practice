package practice.componentfilter;

import java.util.List;

import practice.componentfilter.component.Component;
import practice.componentfilter.component.PriceComponent;
import practice.componentfilter.service.ComponentFilterService;

public class Main {

    private static final List<Component> COMPONENTS = List.of(
            new PriceComponent(10, "1"),
            new PriceComponent(20, "2"),
            new PriceComponent(30, "1")
    );

    public static void main(String[] args) {
        ComponentFilterService service = new ComponentFilterService();
        Integer sum = service.calculateComponentSumForCategoryId(COMPONENTS, "1");
        System.out.println("This should be 40: " + sum);
    }

}
