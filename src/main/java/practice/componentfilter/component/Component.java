package practice.componentfilter.component;

public interface Component {

    Integer getAmount();

    String getCategoryId();
}
