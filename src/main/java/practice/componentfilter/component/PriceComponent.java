package practice.componentfilter.component;

public record PriceComponent(Integer amount, String categoryId) implements Component {

    @Override
    public Integer getAmount() {
        return amount;
    }

    @Override
    public String getCategoryId() {
        return categoryId;
    }
}
