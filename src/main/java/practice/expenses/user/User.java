package practice.expenses.user;

/**
 * Encapsulates user related information.
 */
public record User(Long id, String name) {

}
