package practice.expenses;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Map;

import practice.expenses.expense.Expense;
import practice.expenses.expense.ExpenseOrganizerService;
import practice.expenses.user.UserService;

public class Main {
    private static final LocalDate JANUARY_1 = LocalDate.of(2021, Month.JANUARY, 1);
    private static final LocalDate JANUARY_2 = LocalDate.of(2021, Month.JANUARY, 2);
    private static final Long USER_ID_1 = 1L;
    private static final Long USER_ID_2 = 2L;

    public static void main(String[] args) {
        ExpenseOrganizerService service = new ExpenseOrganizerService(new UserService());

        /*
         * The task:
         *      - implement ExpenseOrganizerService#organize based on the following
         *          - the result map should contain the user's name and the date as key
         *          - the result map should contain the sum of the daily expenses per user as value
         *      - create a class for the map's key to satisfy the criteria above
         *      - ordering and formatting is not important
         *
         * Additional task:
         *      - if a user is not found, collect all daily expenses under the username "other"
         */
        Map<Object, Integer> result = service.organize(getExpenses());
        System.out.println(result);
    }

    /*
     * Example output for this input:
     * "Steve",  2021-01-01 -> 300
     * "Steve",  2021-01-02 -> 150
     * "George", 2021-01-02 -> 250
     */
    private static List<Expense> getExpenses() {
        return List.of(
                createExpense(USER_ID_1, 100, JANUARY_1),
                createExpense(USER_ID_1, 200, JANUARY_1),
                createExpense(USER_ID_1, 150, JANUARY_2),
                createExpense(USER_ID_2, 250, JANUARY_2)
        );
    }

    private static Expense createExpense(Long userId, Integer amount, LocalDate date) {
        return new Expense(userId, amount, date);
    }
}
