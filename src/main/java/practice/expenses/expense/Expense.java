package practice.expenses.expense;

import java.time.LocalDate;

/**
 * Encapsulates information about an expense amount for a given day by a user.
 */
public record Expense(Long userId, Integer amount, LocalDate date) {
}
